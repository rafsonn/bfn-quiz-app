# BFN Quiz

A new Flutter application with build in QR Code scanner and Webview.

## General Info

This application was used at [Bałtycki Festiwal Nauki](https://festiwal.pg.edu.pl/bfn/) (Baltic Science Festival).


<img src="/Screens/loading_screen.png" height="250">
<img src="/Screens/main_screen.png" height="250">
